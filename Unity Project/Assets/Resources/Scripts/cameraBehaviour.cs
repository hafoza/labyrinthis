﻿using UnityEngine;
using System.Collections;

public class cameraBehaviour : MonoBehaviour {

	public Transform target;
	public float[] distance = new float[3];
	public float[] defaultD = new float[3];
	public float xRot;

	public bool canMoveCamera;

	public Vector3 position;
	public Vector3 rotation;
	public Quaternion rot;

	void Awake () {

		canMoveCamera = false;
        Cursor.lockState = CursorLockMode.Locked;

		//set distance to default values
		for(int i=0;i<=2;i++)
			distance[i] = defaultD[i];

	}
	

	void Update () {

		if(canMoveCamera)
		{
			gameObject.transform.position = target.transform.position - new Vector3(0,-1.5F,0);

			xRot -= Input.GetAxis("Mouse Y") * Time.deltaTime * 100;

			xRot = Mathf.Clamp(xRot,-30,30);

			rotation.y = target.rotation.eulerAngles.y;
			rotation.x = xRot;

			rot.eulerAngles = rotation;

			transform.rotation = rot;
		}
	}
}
