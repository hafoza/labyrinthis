﻿using UnityEngine;
using System.Collections;

public class testGenerator : MonoBehaviour
{

    public int width, height, steps;
    public bool finished;
    GameObject obj, instObj, objX, instObjX, cell;
    public mainMenu mm;

    public int[] startCoord, endCoord;

    public enum direction { topRight = 0, right = 1, bottomRight = 2, bottomLeft = 3, left = 4, topLeft = 5 };
    public struct mazeCell
    {
        public bool visited;
        public int[] walls;
        public bool deadEnd;
    };
    public mazeCell[,] thisMaze;

    int[] getUnvisitedCell()
    {
        int x, y;
        int[] z;
        x = Random.Range(0, height);
        y = Random.Range(0, width);
        if (thisMaze[x, y].visited == false)
        {
            z = new int[] { x, y };
            return z;
        }
        else
            return getUnvisitedCell();
    }

    bool checkVisited(int x, int y)
    {
        if (thisMaze[x, y].visited == true)
            return true;
        else
            return false;
    }

    public Vector2 hex_direction(direction x)
    {
        Vector2 Direction = new Vector2(0, 0);

        if (x == direction.topRight)
        {
            Direction.Set(1, -1);
        }
        if (x == direction.right)
        {
            Direction.Set(1, 0);
        }
        if (x == direction.bottomRight)
        {
            Direction.Set(0, 1);
        }
        if (x == direction.bottomLeft)
        {
            Direction.Set(-1, 1);
        }
        if (x == direction.left)
        {
            Direction.Set(-1, 0);
        }
        if (x == direction.topLeft)
        {
            Direction.Set(0, -1);
        }

        return Direction;
    }

    Vector2 getRandomUnvisitedNeighbour(int x, int y)
    {
        int dir = Random.Range(0, 6);
        direction Direction = (direction)dir;
        Vector2 increment = hex_direction(Direction);

        int xIncrement, yIncrement;
        xIncrement = (int)increment.x;
        yIncrement = (int)increment.y;

        Vector2 result;

        result = new Vector2(x + xIncrement, y + yIncrement);

        if (x + xIncrement >= 0 && y + yIncrement >= 0 && x + xIncrement < height && y + yIncrement < width && checkVisited(x + xIncrement, y + yIncrement) == false)
        {
            if (Direction == direction.topRight)
            {
                thisMaze[x + xIncrement, y + yIncrement].walls[(int)direction.bottomLeft] = 0;
                thisMaze[x, y].walls[(int)direction.topRight] = 0;
            }
            if (Direction == direction.right)
            {
                thisMaze[x + xIncrement, y + yIncrement].walls[(int)direction.left] = 0;
                thisMaze[x, y].walls[(int)direction.right] = 0;
            }
            if (Direction == direction.bottomRight)
            {
                thisMaze[x + xIncrement, y + yIncrement].walls[(int)direction.topLeft] = 0;
                thisMaze[x, y].walls[(int)direction.bottomRight] = 0;
            }
            if (Direction == direction.bottomLeft)
            {
                thisMaze[x + xIncrement, y + yIncrement].walls[(int)direction.topRight] = 0;
                thisMaze[x, y].walls[(int)direction.bottomLeft] = 0;
            }
            if (Direction == direction.left)
            {
                thisMaze[x + xIncrement, y + yIncrement].walls[(int)direction.right] = 0;
                thisMaze[x, y].walls[(int)direction.left] = 0;
            }
            if (Direction == direction.topLeft)
            {
                thisMaze[x + xIncrement, y + yIncrement].walls[(int)direction.bottomRight] = 0;
                thisMaze[x, y].walls[(int)direction.topLeft] = 0;
            }
            return result;
        }
        else
            return getRandomUnvisitedNeighbour(x, y);

    }

    bool hasUnvisitedNeighbours(int x, int y)
    {

        if (x + 1 < height && thisMaze[x + 1, y].visited == false)
            return true;
        if (x - 1 >= 0 && thisMaze[x - 1, y].visited == false)
            return true;
        if (y + 1 < width && thisMaze[x, y + 1].visited == false)
            return true;
        if (y - 1 >= 0 && thisMaze[x, y - 1].visited == false)
            return true;
        if (y - 1 >= 0 && x + 1 < height && thisMaze[x + 1, y - 1].visited == false)
            return true;
        if (y + 1 < width && x - 1 >= 0 && thisMaze[x - 1, y + 1].visited == false)
            return true;
        return false;
    }

    void createWalls()
    {
        int i, j, k;
        for (i = 0; i < height; i++)
        {
            for (j = 0; j < width; j++)
            {
                cell = new GameObject();

                cell.name = "Cell_" + i.ToString() + "_" + j.ToString();
                /*objX = (GameObject)Resources.Load("Prefabs/Roof");
                instObjX = (GameObject)Object.Instantiate(objX, new Vector3((j * 6 + objX.transform.position.x), 3 + objX.transform.position.y, (i * 6 + objX.transform.position.z)), objX.transform.rotation);
                instObjX.transform.parent = cell.transform;
                */
                objX = (GameObject)Resources.Load("Prefabs/Hex/hexCell_floor");
                instObjX = (GameObject)Object.Instantiate(objX, new Vector3((i * 6 + objX.transform.position.x), -3 + objX.transform.position.y, (j * -6 + objX.transform.position.z)), objX.transform.rotation);
                instObjX.transform.parent = cell.transform;

                for (k = 0; k < 6; k++)
                {
                    if (thisMaze[i, j].walls[k] == 1)
                    {

                        obj = (GameObject)Resources.Load("Prefabs/Hex/hexCell_" + (direction)k);
                        instObj = (GameObject)Object.Instantiate(obj, new Vector3((i * 6 + obj.transform.position.x), -3 + obj.transform.position.y, (j * -6 + obj.transform.position.z)), obj.transform.rotation);
                        instObj.transform.parent = cell.transform;

                    }

                }
                cell.transform.position += new Vector3(3*j, 0, 0);
                cell.transform.position += new Vector3(0, 0, j * (1.5F));
                cell.transform.parent = GameObject.Find("Layout").transform;
            }

        }
    }

    void generate(int x, int y)
    {
        int i, j;
        steps++;
        for (i = 0; i < height; i++)
            for (j = 0; j < width; j++)
            {
                if (thisMaze[i, j].visited == false)
                    finished = false;
                else
                {
                    finished = true;
                }
            }

        thisMaze[x, y].visited = true;

        if (hasUnvisitedNeighbours(x, y))
        {
            Vector2 neighbour = getRandomUnvisitedNeighbour(x, y);
            //print("Current cell is: " + x + "_" + y + " and proceeding towards: " + (int)neighbour.x + "_" + (int)neighbour.y);
            generate((int)neighbour.x, (int)neighbour.y);
            generate(x, y);
        }
    }


    void Start()
    {

        startCoord = new int[2];
        endCoord = new int[2];
        int i, j, k;
        if (GameObject.Find("GameManager"))
            mm = GameObject.Find("GameManager").GetComponent<mainMenu>();
        //Default Values
        if (width == 0 && height == 0)
        {
            width = 30;
            height = 20;
        }
        //If settings are reachable
        if (mm)
        {
            int[] dimensions = mm.getLevelDimensions();
            height = dimensions[0];
            width = dimensions[1];
            Destroy(mm);
        }

        thisMaze = new mazeCell[height, width];

        for (i = 0; i < height; i++)
            for (j = 0; j < width; j++)
            {
                thisMaze[i, j].walls = new int[6];
                for (k = 0; k < 6; k++)
                    thisMaze[i, j].walls[k] = 1;
            }
        while (!finished)
        {
            int[] z = getUnvisitedCell();

            generate(z[0], z[1]);
        }

        for (i = 0; i < height; i++)
        {
            for (j = 0; j < width; j++)
            {
                int ok = 0;
                for (k = 0; k < 6; k++)
                {
                    if (thisMaze[i, j].walls[k] == 0)
                    {
                        if (ok == 0)
                            ok = 1;
                        else
                            break;
                    }
                }
                if (ok == 1)
                    thisMaze[i, j].deadEnd = true;
            }
        }
        thisMaze[0, 0].walls[4] = 0;
        createWalls();

    }
}
