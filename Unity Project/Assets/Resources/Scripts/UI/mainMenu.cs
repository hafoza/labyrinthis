﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class mainMenu : MonoBehaviour {

    int levelHeight;
    int levelWidth;

    public CanvasGroup UIMainMenu, UIProfileCreation, UIProfileLoading, current;

    public int[] getLevelDimensions()
    {
        if (levelHeight == 0 && levelWidth == 0)
            return new int[] { 20, 20 };
        else
            return new int[] { levelHeight, levelWidth };
    }

    public void toggleMainMenu()
    {
        toggleCanvasGroup(current);
        toggleCanvasGroup(UIMainMenu);
        current = UIMainMenu;
    }

    public void toggleProfileLoading()
    {
        toggleCanvasGroup(current);
        toggleCanvasGroup(UIProfileLoading);
        current = UIProfileLoading;
    }

    public void toggleProfileCreation()
    {
        toggleCanvasGroup(current);
        toggleCanvasGroup(UIProfileCreation);
        current = UIProfileCreation;
    }

    void toggleCanvasGroup(CanvasGroup cg)
    {
        if (cg.alpha == 1)
        {
            cg.alpha = 0;
            cg.interactable = false;
            cg.blocksRaycasts = false;
        }
        else
        {
            cg.alpha = 1;
            cg.interactable = true;
            cg.blocksRaycasts = true;
        }
    }

    public void exitGame()
    {
        Application.Quit();
    }

    void Start()
    {
        current = UIMainMenu;
        levelWidth = levelHeight = 20;
    }

}
