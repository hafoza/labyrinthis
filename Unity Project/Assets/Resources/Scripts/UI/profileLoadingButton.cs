﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class profileLoadingButton : MonoBehaviour, IPointerDownHandler {

    public string profileName;
    public ProfileManager ProfileManager;

    public void OnPointerDown(PointerEventData eventData)
    {
        ProfileManager.LoadProfileInfo(profileName);
        ProfileManager.currentProfile.loadGame();
    }

}
