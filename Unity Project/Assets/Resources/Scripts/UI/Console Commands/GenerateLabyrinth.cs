﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerateLabyrinth : IConsoleCommand<List<string>> {

    private string _name;
    private string _failMessage;
    private string _successMessage;

    public string commandName
    {
        get
        {
            return _name;
        }
    }
    public string failMessage
    {
        get
        {
            return _failMessage;
        }
    }
    public string successMessage
    {
        get
        {
            return _successMessage;
        }
    }

    public GenerateLabyrinth()
    {
        _name = "GenerateLabyrinth";
        _failMessage = "Failed to generate a new maze.";
        _successMessage = "Successfully generated a new maze.";
    }

    public bool triggerCommand(List<string> parameters)
    {
        GameManager.instance.GetComponent<LevelGeneration>().cleanLayout();
        GameManager.instance.GetComponent<LevelGeneration>().generateMaze();
        GameManager.instance.playerAvatar.transform.position = new Vector3(0, 1.08F, 0);
        return true;
    }

}
