﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SaveProfileInfo : IConsoleCommand<List<string>>
{
    private string _name;
    private string _failMessage;
    private string _successMessage;
    private ProfileManager ProfileManager;

    public string commandName
    {
        get
        {
            return _name;
        }
    }
    public string failMessage
    {
        get
        {
            return _failMessage;
        }
    }
    public string successMessage
    {
        get
        {
            return _successMessage;
        }
    }

    public SaveProfileInfo()
    {
        _name = "SaveProfileInfo";
        _failMessage = "Failed to save profile.";
        _successMessage = "Successfully saved profile.";
        ProfileManager = GameObject.FindGameObjectWithTag("ProfileManager").GetComponent<ProfileManager>();
    }

    public bool triggerCommand(List<string> parameters)
    {
        ProfileManager.SaveProfileInfo(ProfileManager.currentProfile);
        return true;
    }

}
