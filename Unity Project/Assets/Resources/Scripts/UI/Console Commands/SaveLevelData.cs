﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SaveLevelData : IConsoleCommand<List<string>> 
{
    private string _name;
    private string _failMessage;
    private string _successMessage;
    private GameObject GameManager;
    private ProfileManager ProfileManager;

    public string commandName
    {
        get
        {
            return _name;
        }
    }
    public string failMessage
    {
        get
        {
            return _failMessage;
        }
    }
    public string successMessage
    {
        get
        {
            return _successMessage;
        }
    }

    public SaveLevelData()
    {
        _name = "SaveLevelData";
        _failMessage = "Failed to save level data.";
        _successMessage = "Successfully saved level data.";
        GameManager = GameObject.FindGameObjectWithTag("GameManager");
        ProfileManager = GameObject.FindGameObjectWithTag("ProfileManager").GetComponent<ProfileManager>();
    }

    public bool triggerCommand(List<string> parameters)
    {
        GameManager.GetComponent<Serialization>().SaveLevelData(ProfileManager.currentProfile);
        return true;
    }


}
