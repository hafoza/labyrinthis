﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReplaceLevel : IConsoleCommand<List<string>>
{

	private string _name;
    private string _failMessage;
    private string _successMessage;
    private GameObject GameManager;
    private ProfileManager ProfileManager;

    public string commandName
    {
        get
        {
            return _name;
        }
    }
    public string failMessage
    {
        get
        {
            return _failMessage;
        }
    }
    public string successMessage
    {
        get
        {
            return _successMessage;
        }
    }

    public ReplaceLevel()
    {
        _name = "ReplaceLevel";
        _failMessage = "Failed to replace level.";
        _successMessage = "Successfully replaced level.";
        GameManager = GameObject.FindGameObjectWithTag("GameManager");
        ProfileManager = GameObject.FindGameObjectWithTag("ProfileManager").GetComponent<ProfileManager>();
    }

    public bool triggerCommand(List<string> parameters)
    {
        GameManager.GetComponent<Serialization>().replaceLevel(
            ProfileManager.currentProfile.getCurrentLevel(), 
            GameManager.GetComponent<LevelGeneration>().thisMaze, 
            ProfileManager.currentProfile);
        return true;
    }

}
