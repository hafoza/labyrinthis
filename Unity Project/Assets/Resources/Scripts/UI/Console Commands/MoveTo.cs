﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveTo : IConsoleCommand<List<string>> {

    Transform characterTransform;
    private string _name;
    private string _failMessage;
    private string _successMessage;
    private GameObject gameManager;
    private List<string> specialImputs;

    public string commandName
    {
        get 
        {
            return _name;
        }
    }
    public string failMessage
    {
        get
        {
            return _failMessage;
        }
    }
    public string successMessage
    {
        get
        {
            return _successMessage;
        }
    }
    public MoveTo()
    {
        _name = "MoveTo";
        _failMessage = "Command MoveTo needs a cell input (int, int), a Vector3 input (int, int, int), or a special input (exit, entrance)";

        specialImputs = new List<string>();
        specialImputs.Add("entrance");
        specialImputs.Add("exit");

        gameManager = GameManager.instance.gameObject;
    }

    public bool triggerCommand(List<string> parameters)
    {
        levelData lData = gameManager.GetComponent<LevelGeneration>().thisMaze;

        switch (parameters.Count)
        {
            case 0:
                //default command does nothing
                return false;
            case 1:
                string specialInput = parameters[0];
                if (specialImputs.Contains(specialInput))
                {
                    switch (specialInput)
                    {
                        case "entrance":
                            gameManager.GetComponent<GameManager>().playerAvatar.transform.position = new Vector3(0, 1.08F, 0);
                            break;
                        case "exit":

                            gameManager.GetComponent<GameManager>().playerAvatar.transform.position = new Vector3(lData.exitCoord[0] * 6, 1.08F, lData.exitCoord[1] * 6);
                            break;
                    }
                    _successMessage = "Successfully moved to " + specialInput + ".";
                    return true;
                }
                else
                {
                    _failMessage = "The special input " + specialInput + " is not valid.";
                    return false;
                }
            case 2:
                int x = int.Parse(parameters[0]);
                int y = int.Parse(parameters[1]);

                if (x <= lData.mazeLayout.GetLength(0) && x >= 0 && y <= lData.mazeLayout.GetLength(1) && y >= 0)
                {
                    gameManager.GetComponent<GameManager>().playerAvatar.transform.position = new Vector3((float)x * 6, 0.5F, (float)y * 6);
                    _successMessage = "Succesfully moved to cell" + x + "_" + y + ".";
                    return true;
                }
                else
                {
                    _failMessage = "You have entered an invalid cell input.";
                    return false;
                }
            case 3:
                int _x = int.Parse(parameters[0]);
                int _y = int.Parse(parameters[1]);
                int _z = int.Parse(parameters[2]);
                gameManager.GetComponent<GameManager>().playerAvatar.transform.position = new Vector3(_x, _y, _z);
                _successMessage = "Succesfully moved to coordinates " + _x + ", " + _y + ", " + _z + ".";
                return true;
        }
        return false;
    }

}
