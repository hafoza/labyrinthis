﻿using System.Collections;

public interface IConsoleCommand<T> {

    string commandName
    {
        get;
    }

    string failMessage
    {
        get;
    }

    string successMessage
    {
        get;
    }

    bool triggerCommand(T parameters);
}
