﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GoToLevel : IConsoleCommand<List<string>> {

    private string _name;
    private string _failMessage;
    private string _successMessage;
    private ProfileManager ProfileManager;

    public string commandName
    {
        get
        {
            return _name;
        }
    }
    public string failMessage
    {
        get
        {
            return _failMessage;
        }
    }
    public string successMessage
    {
        get
        {
            return _successMessage;
        }
    }

    public GoToLevel()
    {
        _name = "GoToLevel";
        _failMessage = "Command GoToLevel requires a level input (int).";
        _successMessage = "Successfully loaded level.";
        ProfileManager = GameObject.FindGameObjectWithTag("ProfileManager").GetComponent<ProfileManager>();
    }

    public bool triggerCommand(List<string> parameters)
    {
        switch (parameters.Count)
        {
            case 0:
                //default command does nothing
                return false;
            case 1:
                                int level = int.Parse(parameters[0]);
                if (level >= 0 && ProfileManager.currentProfile.highestLevelGenerated >= level)
                {
                    ProfileManager.currentProfile.goToLevel((int)level);
                    return true;
                }
                else
                {
                    _failMessage = "You have entered an invalid input for the level.";
                    return false;
                }
        }
        return false;
    }

}
