﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisplayProfileInfo : IConsoleCommand<List<string>>
{
    private string _name;
    private string _failMessage;
    private string _successMessage;
    private ProfileManager ProfileManager;

    public string commandName
    {
        get
        {
            return _name;
        }
    }
    public string failMessage
    {
        get
        {
            return _failMessage;
        }
    }
    public string successMessage
    {
        get
        {
            return _successMessage;
        }
    }

    public DisplayProfileInfo()
    {
        _name = "DisplayProfileInfo";
        _failMessage = "Failed to display profile info. Check if your scene has a profile loaded.";
        _successMessage = "Displaying profile info...\n";
        ProfileManager = GameObject.FindGameObjectWithTag("ProfileManager").GetComponent<ProfileManager>();
    }

    public bool triggerCommand(List<string> parameters)
    {
        _successMessage += (ProfileManager.currentProfile.returnProfileData());
        return true;
    }


}
