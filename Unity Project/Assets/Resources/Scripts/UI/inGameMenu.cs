using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class inGameMenu : MonoBehaviour
{
    /*
    public bool showMenu;
    public bool onMM;
    public bool onOS;
    public cameraBehaviour cam;
    public GUIStyle style;


    void Start()
    {

        showMenu = false;
        onMM = true;
        onOS = false;
        style.normal.textColor = Color.white;
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (showMenu)
            {
                Cursor.lockState = CursorLockMode.Locked;
                showMenu = false;
                Time.timeScale = 1;
                cam.canMoveCamera = true;
            }
            else
            {
                cam.canMoveCamera = false;
                showMenu = true;
                Time.timeScale = 0;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                onOS = false;
                onMM = true;
            }
        }

    }


    void OnGUI()
    {


        if (showMenu)
        {
            GUI.Box(new Rect(-2, -2, Screen.width + 5, Screen.height + 5), "");
            if (onMM)
            {
                GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 250));

                if (GUILayout.Button("Resume"))
                {
                    cam.canMoveCamera = true;
                    Cursor.lockState = CursorLockMode.Locked;
                    showMenu = false;
                    Time.timeScale = 1;
                }

                GUILayout.Space(25);

                if (GUILayout.Button("Options"))
                {
                    onMM = false;
                    onOS = true;
                }

                GUILayout.Space(25);

                if (GUILayout.Button("Exit to Main Menu"))
                {
                    Time.timeScale = 1;
                    SceneManager.LoadScene("Main Menu");
                }

                GUILayout.Space(25);

                if (GUILayout.Button("Exit to Desktop"))
                {
                    Application.Quit();
                }
                GUILayout.EndArea();
            }

            if (onOS)
            {
                GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 100, 200, 250));
                GUILayout.Label("Options Screen is in work!", style);

                GUILayout.Space(25);

                if (GUILayout.Button("Back"))
                {
                    onMM = true;
                    onOS = false;
                }
                GUILayout.EndArea();
            }
        }

    }
     */

    public CanvasGroup menuCanvasGroup;

    public void toggleMenuVisibility()
    {
        if (menuCanvasGroup.alpha == 1)
        {
            menuCanvasGroup.alpha = 0;
            menuCanvasGroup.interactable = false;
            menuCanvasGroup.blocksRaycasts = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = 1;
        }
        else
        {
            menuCanvasGroup.alpha = 1;
            menuCanvasGroup.interactable = true;
            menuCanvasGroup.blocksRaycasts = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0;
        }
    }

    public void returnToMainMenu()
    {
        GameObject.Destroy(GameObject.FindGameObjectWithTag("ProfileManager"));
        GameObject.Destroy(GameManager.instance);
        SceneManager.LoadScene("Main Menu");
    }

    public void exitGame()
    {
        Application.Quit();
    }

    void Start()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            toggleMenuVisibility();
        }
    }
}
