﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class GameConsole : MonoBehaviour {

    public InputField commandInput;
    public CanvasGroup console;
    public Text log;
    public Dictionary<string, string> commandHelp;
    public IConsoleCommand<List<string>> command;

    public void triggerCommandEffect(string name, List<string> parameters)
    {
        string temp = log.text;

        if (name == "moveto")
        {
            command = new MoveTo();
        }
        else if (name == "gotolevel")
        {
            command = new GoToLevel();
        }
        else if (name == "replacelevel")
        {
            command = new ReplaceLevel();
        }
        else if (name == "generatelabyrinth")
        {
            command = new GenerateLabyrinth();
        }
        else if (name == "saveprofileinfo")
        {
            command = new SaveProfileInfo();
        }
        else if (name == "saveleveldata")
        {
            command = new SaveLevelData();
        }
        else if (name == "displayprofileinfo")
        {
            command = new DisplayProfileInfo();
        }
        else if (name == "help")
        {
            showHelp(parameters);
            return;
        }
        else
        {
            log.text = '\n' + "Invalid input.";
            log.text += temp;
            return;
        }

        if (command.triggerCommand(parameters))
            log.text = '\n' +command.successMessage + "\n";
        else
            log.text = '\n' + command.failMessage + "\n";

        log.text += temp;
    }

    public void showHelp(List<string> parameters)
    {
        if(parameters.Count<1)
            {
                foreach (var key in commandHelp.Keys)
                {
                    log.text += string.Format("{0} -> {1}\n", key, commandHelp[key]);
                }
            }
        else
        {
            log.text += string.Format("{0} -> {1}\n", parameters[0], commandHelp[parameters[0]]);
        }
            commandInput.text = "";
       log.text += "\n";
    }

    public void triggerCommand()
    {
        if(commandInput.text != "")
        { 
            string[] temp, parametersString;
            List<string> parametersList = new List<string>();
        
            commandInput.text.Replace(" ", "");
            temp = commandInput.text.Split(new char[]{'(',')'});
            if(temp.Length > 1)
            {
                if (temp[1].Split(',') != null)
                {
                    parametersString = temp[1].Split(new char []{' ', ','});

                    for (int i = 0; i < parametersString.Length; i++)
                    {
                        parametersList.Add(parametersString[i]);
                    }
                }
                else
                    parametersList.Add(temp[1]);
            }
            triggerCommandEffect(temp[0].ToLower(), parametersList);

            commandInput.text = "";
        }
    }

    public void toggleConsole()
    {
        if (console.alpha == 1)
        {
            Time.timeScale = 1;
            console.alpha = 0;
            console.interactable = false;
            console.blocksRaycasts = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Time.timeScale = 0;
            console.alpha = 1;
            console.interactable = true;
            console.blocksRaycasts = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public static void consoleLog(string data)
    {
        GameConsole gc;
        gc = GameObject.FindGameObjectWithTag("Console").GetComponent<GameConsole>();
        string temp = gc.log.text;
        gc.log.text = "\n";
        gc.log.text += data;
        gc.log.text += temp;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
            toggleConsole();
    }

    void Start()
    {
        commandHelp = new Dictionary<string,string>();
        commandHelp.Add("saveleveldata", "Saves current level data to file.");
        commandHelp.Add("generatelabyrinth", "Generates a new labyrinth layout for the current level.");
        commandHelp.Add("gotolevel", "Loads a different level generated on the current profile. The level must already be generated for this command to work.");
        commandHelp.Add("moveto", "Moves the player character to specified coordinates or special input. (enter/exit)");
        commandHelp.Add("saveprofileinfo", "Saves current profile info.");
        commandHelp.Add("displayprofileinfo", "Displays the following profile info: name, id, current level, current character position, highest level reached, and highest level generated.");
    }

}
