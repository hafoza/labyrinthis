﻿using UnityEngine;
using System.Collections;

public class stats : MonoBehaviour {
	
	public float movementSpeed,runSpeed,jumpHeight,gravityForce,rotationSpeed;
	public float[] movementStats = new float[5];

	public float protection;
	
	void Start () {

	movementStats[0] = movementSpeed;
	movementStats[1] = runSpeed;
	movementStats[2] = jumpHeight;
	movementStats[3] = gravityForce;
	movementStats[4] = rotationSpeed;

	}

	public float[] sendMovementStats(){
		return movementStats;	
	}
}
