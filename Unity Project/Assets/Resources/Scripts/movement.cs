﻿using UnityEngine;
using System.Collections;

	[System.Serializable]
	public class movementStats
	{
		
		public float mS,rS,jH,gF,rotS;
		public bool jumping;
		public movementStats(float[] statsArr)
		{
			mS = statsArr[0];
			rS = statsArr[1];
			jH = statsArr[2];
			gF = statsArr[3];
			rotS = statsArr[4];
		}
		
	};
	

public class movement : MonoBehaviour {

	public bool canMove;
	public movementStats mStats;
	public CharacterController controller;
	public Vector3 movementDirection;
	public Quaternion rotation;
	public float horizontalRot;
	
	void Start () {
		controller = GetComponent<CharacterController>();
		init();
		canMove = true;
		cameraBehaviour cam = Camera.main.GetComponent<cameraBehaviour>();
		cam.canMoveCamera = true;
	}

	void Update () {

		if(mStats.mS == 0)
		{
			init();
		}

		if(canMove)
		{
            
			if(controller.isGrounded)
			{
				movementDirection.y = 0;
				if(Input.GetButton("Run"))
				{
					movementDirection.z = Input.GetAxis("Vertical") * mStats.rS;
					movementDirection.x = Input.GetAxis("Horizontal") * mStats.rS;
				}
				else
				{
					movementDirection.z = Input.GetAxis("Vertical") * mStats.mS;
					movementDirection.x = Input.GetAxis("Horizontal") * mStats.mS;
				}
				movementDirection = transform.TransformDirection(movementDirection);
				if(Input.GetButton("Jump"))
				{
					movementDirection.y = mStats.jH;
					mStats.jumping = true;
				}
				else
					mStats.jumping = false;
			}
			else
				movementDirection.y -= mStats.gF * Time.deltaTime;

			controller.Move(movementDirection * Time.deltaTime);

			transform.Rotate(new Vector3(0,(Input.GetAxis("Mouse X") * Time.deltaTime * mStats.rotS),0));
            
		}

	}

	public void init(){
		stats charStats = gameObject.GetComponent<stats>();
		float[] statsArr = new float[5];
		statsArr = charStats.sendMovementStats();
		mStats = new movementStats(statsArr);
	}
}
