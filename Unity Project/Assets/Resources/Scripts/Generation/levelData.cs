﻿using System.Collections;

[System.Serializable()]
public class levelData
{

    [System.Serializable()]
    public struct mazeCell
    {
        public bool visited;
        public int[] walls;
        public bool deadEnd;
        public string specialAttribute;
    };
    public mazeCell[,] mazeLayout;

    int lIndex;
    bool levelInitialized;
    public int[] exitCoord;
    public int exitOrientation;

    public void setMatrixToSerialize(mazeCell[,] matrixToSerialize, int levelIndex)
    {
        mazeLayout = matrixToSerialize;
        lIndex = levelIndex;
    }

    public mazeCell[,] returnLayout()
    {
        return mazeLayout;
    }

    public void setInitialized()
    {
        levelInitialized = true;
    }

    public int returnLevel()
    {
        return lIndex;
    }

    public bool checkMatrixInitialized()
    {
        return levelInitialized;
    }
    public void setLevelIndex(int level)
    {
        lIndex = level;
    }
}