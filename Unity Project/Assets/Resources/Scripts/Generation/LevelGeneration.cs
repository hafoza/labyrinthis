﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using UnityEngine.SceneManagement;

public class LevelGeneration : MonoBehaviour
{

    public int width, height, steps;
    public bool finished;
    GameObject obj, instObj, objX, instObjX, cell, layout;
    public Vector2 startCoord, endCoord;

    public enum direction { up = 0, right = 1, down = 2, left = 3};
    public levelData thisMaze;
    public ProfileManager ProfileManager;

    int[] getUnvisitedCell()
    {
        int x, y;
        int[] z;
        x = Random.Range(0, height);
        y = Random.Range(0, width);
        if (thisMaze.mazeLayout[x, y].visited == false)
        {
            z = new int[] { x, y };
            return z;
        }
        else
            return getUnvisitedCell();
    }

    bool checkVisited(int x, int y)
    {
        if (thisMaze.mazeLayout[x, y].visited == true)
            return true;
        else
            return false;
    }

    public Vector2 square_direction(direction x)
    {
        Vector2 Direction = new Vector2(0, 0);

        if (x == direction.right)
        {
            Direction.Set(1, 0);
        }
        if (x == direction.left)
        {
            Direction.Set(-1, 0);
        }
        if (x == direction.up)
        {
            Direction.Set(0, 1);
        }
        if (x == direction.down)
        {
            Direction.Set(0, -1);
        }
        return Direction;
    }

    Vector2 getRandomUnvisitedNeighbour(int x, int y)
    {
        int dir = Random.Range(0, 4);
        direction Direction = (direction)dir;
        Vector2 increment = square_direction(Direction);

        int xIncrement, yIncrement;
        xIncrement = (int)increment.x;
        yIncrement = (int)increment.y;

        Vector2 result;

        result = new Vector2(x + xIncrement, y + yIncrement);

        if (x + xIncrement >= 0 && y + yIncrement >= 0 && x + xIncrement < height && y + yIncrement < width && checkVisited(x + xIncrement, y + yIncrement) == false)
        {
            if (Direction == direction.up)
            {
                thisMaze.mazeLayout[x + xIncrement, y + yIncrement].walls[(int)direction.down] = 0;
                thisMaze.mazeLayout[x, y].walls[(int)direction.up] = 0;
            }
            if (Direction == direction.right)
            {
                thisMaze.mazeLayout[x + xIncrement, y + yIncrement].walls[(int)direction.left] = 0;
                thisMaze.mazeLayout[x, y].walls[(int)direction.right] = 0;
            }
            if (Direction == direction.down)
            {
                thisMaze.mazeLayout[x + xIncrement, y + yIncrement].walls[(int)direction.up] = 0;
                thisMaze.mazeLayout[x, y].walls[(int)direction.down] = 0;
            }
            if (Direction == direction.left)
            {
                thisMaze.mazeLayout[x + xIncrement, y + yIncrement].walls[(int)direction.right] = 0;
                thisMaze.mazeLayout[x, y].walls[(int)direction.left] = 0;
            }
            return result;
        }
        else
            return getRandomUnvisitedNeighbour(x, y);

    }

    bool hasUnvisitedNeighbours(int x, int y)
    {

        if (x + 1 < height && thisMaze.mazeLayout[x + 1, y].visited == false)
            return true;
        if (x - 1 >= 0 && thisMaze.mazeLayout[x - 1, y].visited == false)
            return true;
        if (y + 1 < width && thisMaze.mazeLayout[x, y + 1].visited == false)
            return true;
        if (y - 1 >= 0 && thisMaze.mazeLayout[x, y - 1].visited == false)
            return true;
        return false;
    }

    /*
    void createWalls()
    {
        int i, j, k;
        for (i = 0; i < height; i++)
        {
            for (j = 0; j < width; j++)
            {
                cell = new GameObject();

                cell.name = "Cell_" + i.ToString() + "_" + j.ToString();
                if(thisMaze.mazeLayout[i,j].specialAttribute == "none")
                { 
                    objX = (GameObject)Resources.Load("Prefabs/Square/squareCell_floor");
                    instObjX = (GameObject)Object.Instantiate(objX, new Vector3((i * 6 + objX.transform.position.x), objX.transform.position.y, (j * 6 + objX.transform.position.z)), objX.transform.rotation);
                    instObjX.transform.parent = cell.transform;
                }
                for (k = 0; k < 4; k++)
                {
                    if (thisMaze.mazeLayout[i, j].walls[k] == 1)
                    {

                        obj = (GameObject)Resources.Load("Prefabs/Square/squareCell_" + (direction)k);
                        instObj = (GameObject)Object.Instantiate(obj, new Vector3((i * 6 + obj.transform.position.x), obj.transform.position.y, (j * 6 + obj.transform.position.z)), obj.transform.rotation);
                        instObj.transform.parent = cell.transform;

                    }

                }
                cell.transform.parent = layout.transform;
            }

        }
    }*/

    void createWalls()
    {
        MaterialPropertyBlock props = new MaterialPropertyBlock();
        MeshRenderer renderer;

        int i, j, k;
        for (i = 0; i < height; i++)
        {
            for (j = 0; j < width; j++)
            {
                cell = new GameObject();

                cell.name = "Cell_" + i.ToString() + "_" + j.ToString();
                if (thisMaze.mazeLayout[i, j].specialAttribute == "none")
                {
                    objX = (GameObject)Resources.Load("Prefabs/Square/squareCell_floor");
                    instObjX = (GameObject)Object.Instantiate(objX, new Vector3((i * 6 + objX.transform.position.x), objX.transform.position.y, (j * 6 + objX.transform.position.z)), objX.transform.rotation);
                    instObjX.transform.parent = cell.transform;

                    float r = Random.Range(0.0f, 1.0f);
                    float g = Random.Range(0.0f, 1.0f);
                    float b = Random.Range(0.0f, 1.0f);
                    props.SetColor("_Color", new Color(r, g, b));

                    renderer = instObjX.GetComponent<MeshRenderer>();
                    //renderer.SetPropertyBlock(props);

                }
                for (k = 0; k < 4; k++)
                {
                    if (thisMaze.mazeLayout[i, j].walls[k] == 1)
                    {

                        //obj = (GameObject)Resources.Load("Prefabs/Square/squareCell_wall");
                        obj = (GameObject)Resources.Load("Prefabs/Square/wall_plane");
                        instObj = (GameObject)Object.Instantiate(obj, new Vector3((i * 6 + obj.transform.position.x), obj.transform.position.y, (j * 6 + obj.transform.position.z)), Quaternion.Euler(270, 90 * k, 0));

                        float r = Random.Range(0.0f, 1.0f);
                        float g = Random.Range(0.0f, 1.0f);
                        float b = Random.Range(0.0f, 1.0f);
                        props.SetColor("_Color", new Color(r, g, b));

                        renderer = instObj.GetComponent<MeshRenderer>();
                        //renderer.SetPropertyBlock(props);
                        
                        instObj.name = "squareCell_";
                       
                        switch (k){
                            case 0:
                                instObj.name += "up";
                                break;
                            case 1:
                                instObj.name += "right";
                                break;
                            case 2:
                                instObj.name += "down";
                                break;
                            case 3:
                                instObj.name += "left";
                                break;
                        }

                        instObj.transform.parent = cell.transform;

                    }

                }
                cell.transform.parent = layout.transform;
            }

        }
    }

    void generate(int x, int y)
    {
        int i, j;
        steps++;
        for (i = 0; i < height; i++)
            for (j = 0; j < width; j++)
            {
                if (thisMaze.mazeLayout[i, j].visited == false)
                {
                    finished = false;
                    break;
                }
                else
                {
                    finished = true;
                }
            }

        thisMaze.mazeLayout[x, y].visited = true;

        if (hasUnvisitedNeighbours(x, y))
        {
            Vector2 neighbour = getRandomUnvisitedNeighbour(x, y);
            generate((int)neighbour.x, (int)neighbour.y);
            generate(x, y);
        }
    }

    public void generateMaze()
    {
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();
        int i, j, k;
        finished = false;
        steps = 0;

        thisMaze.mazeLayout = new levelData.mazeCell[height, width];
        for (i = 0; i < height; i++)
            for (j = 0; j < width; j++)
                thisMaze.mazeLayout[i, j].walls = new int[4];

        for (i = 0; i < height; i++)
            for (j = 0; j < width; j++)
            {
                thisMaze.mazeLayout[i, j].specialAttribute = "none";
                thisMaze.mazeLayout[i, j].visited = false;
                thisMaze.mazeLayout[i, j].deadEnd = false;
                for (k = 0; k < 4; k++)
                    thisMaze.mazeLayout[i, j].walls[k] = 1;
            }

        while (!finished)
        {
            int[] z = getUnvisitedCell();

            generate(z[0], z[1]);
        }

        for (i = 0; i < height; i++)
        {
            for (j = 0; j < width; j++)
            {
                if (checkWallCount(i,j) == 3)
                    thisMaze.mazeLayout[i, j].deadEnd = true;
            }
        }
        thisMaze.mazeLayout[0, 0].walls[3] = 0;
       
        generateExit();

        createWalls();
        stopWatch.Stop();

        GameConsole.consoleLog(string.Format("Finished generating maze in {0:0.0} seconds", (float)stopWatch.ElapsedMilliseconds / 1000));
    }

    int checkWallCount(int i, int j)
    {
        int wallCount = 0;
        for (int k = 0; k < 4; k++)
                {
                    if (thisMaze.mazeLayout[i, j].walls[k] == 1)
                    {
                        wallCount++;
                    }
                }
        return wallCount;
    }

    void generateExit()
    {
        int i, j, k;
        bool foundExit = false;

        for (i = height-1; i >= 0  && !foundExit; i--)
            for (j = width-1; j >= 0 && !foundExit; j--)
                if (thisMaze.mazeLayout[i, j].deadEnd)  //checking if the dead end I found is in a good place
                    for(k = 0; k < 4 && !foundExit; k++)
                        if (thisMaze.mazeLayout[i, j].walls[k] == 0)
                        {
                            direction Direction = (direction)k;
                            Vector2 increment = square_direction(Direction);

                            int incrI = i + (int)increment.x;
                            int incrJ = j + (int)increment.y;

                            if (incrI + (int)increment.x <= height && incrI + (int)increment.x >= 0 && incrJ + (int)increment.y <= width && incrJ + (int)increment.y >=0)
                                if (thisMaze.mazeLayout[incrI, incrJ].walls[k] == 0 && checkWallCount(incrI, incrJ) == 2 && thisMaze.mazeLayout[incrI + (int)increment.x, incrJ + (int)increment.y].walls[k] == 0)
                                {
                                    thisMaze.exitOrientation = k;
                                    foundExit = true;
                                    endCoord.x = incrI;
                                    endCoord.y = incrJ;
                                    thisMaze.mazeLayout[i + (int)increment.x, j + (int)increment.y].specialAttribute = "exitOrigin";
                                    thisMaze.mazeLayout[i, j].specialAttribute = "exitEnd";
                                   
                                }

                        }
        if (!foundExit)
            generateMaze();
        else
        {
            objX = (GameObject)Resources.Load("Prefabs/Square/squareCell_exit");
            if (thisMaze.exitOrientation == 0)
                instObjX = (GameObject)Object.Instantiate(objX, new Vector3((endCoord.x * 6), 0, (endCoord.y * 6) + 3), Quaternion.Euler(270, 90 * thisMaze.exitOrientation, 0));
            if (thisMaze.exitOrientation == 1)
                instObjX = (GameObject)Object.Instantiate(objX, new Vector3((endCoord.x * 6) + 3, 0, (endCoord.y * 6)), Quaternion.Euler(270, 90 * thisMaze.exitOrientation, 0));
            if (thisMaze.exitOrientation == 2)
                instObjX = (GameObject)Object.Instantiate(objX, new Vector3((endCoord.x * 6), 0, (endCoord.y * 6) - 3), Quaternion.Euler(270, 90 * thisMaze.exitOrientation, 0));
            if (thisMaze.exitOrientation == 3)
                instObjX = (GameObject)Object.Instantiate(objX, new Vector3((endCoord.x * 6) - 3, 0, (endCoord.y * 6)), Quaternion.Euler(270, 90 * thisMaze.exitOrientation, 0));
            instObjX.transform.parent = layout.transform;
        }
    }

    void generateExit(Vector2 exitCoordinates)
    {
        endCoord = exitCoordinates;
        objX = (GameObject)Resources.Load("Prefabs/Square/squareCell_exit");
        if (thisMaze.exitOrientation == 0)
            instObjX = (GameObject)Object.Instantiate(objX, new Vector3((endCoord.x * 6), 0, (endCoord.y * 6) + 3), Quaternion.Euler(270, 90 * thisMaze.exitOrientation, 0));
        if (thisMaze.exitOrientation == 1)
            instObjX = (GameObject)Object.Instantiate(objX, new Vector3((endCoord.x * 6) + 3, 0, (endCoord.y * 6)), Quaternion.Euler(270, 90 * thisMaze.exitOrientation, 0));
        if (thisMaze.exitOrientation == 2)
            instObjX = (GameObject)Object.Instantiate(objX, new Vector3((endCoord.x * 6), 0, (endCoord.y * 6) - 3), Quaternion.Euler(270, 90 * thisMaze.exitOrientation, 0));
        if (thisMaze.exitOrientation == 3)
            instObjX = (GameObject)Object.Instantiate(objX, new Vector3((endCoord.x * 6) - 3, 0, (endCoord.y * 6)), Quaternion.Euler(270, 90 * thisMaze.exitOrientation, 0));
        instObjX.transform.parent = layout.transform;
    }

    public void cleanLayout()
    {
        foreach (Transform child in layout.GetComponentsInChildren<Transform>())
        {
            if(child.gameObject != layout)
                Destroy(child.gameObject);
        }
    }

    void loadLevel()
    {
        Serialization serialization = GetComponent<Serialization>();
        levelData lvlData;
        lvlData = serialization.DeserializeLevel(ProfileManager.currentProfile.getCurrentLevel(), ProfileManager.currentProfile.name);
        thisMaze = lvlData;

        generateExit(new  Vector2(thisMaze.exitCoord[0], thisMaze.exitCoord[1]));
        createWalls();
    }

    public void serializeNewLevel()
    {
        generateMaze();
        Serialization serialization = GetComponent<Serialization>();
        thisMaze.setLevelIndex(ProfileManager.currentProfile.getCurrentLevel());
        thisMaze.exitCoord = new int[2]{(int)endCoord.x,(int)endCoord.y};
        ProfileManager.currentProfile.highestLevelGenerated = thisMaze.returnLevel();
        ProfileManager.SaveProfileInfo(ProfileManager.currentProfile);
        serialization.SerializeLevel(thisMaze, ProfileManager.currentProfile.name);
    }

    public void OnSceneChange(Scene scene, Scene newScene)
    {
        layout = GameObject.Find("Layout");
        if (newScene.buildIndex == 1)
        {
            if (ProfileManager.currentProfile.highestLevelGenerated < ProfileManager.currentProfile.getCurrentLevel())
                serializeNewLevel();
            else
                loadLevel();
        }
    }

    void Start()
    {
        ProfileManager = GameObject.FindGameObjectWithTag("ProfileManager").GetComponent<ProfileManager>();
        //Default Values
        if (width == 0 && height == 0)
        {
            width = 30;
            height = 20;
        }
         
    }
}
