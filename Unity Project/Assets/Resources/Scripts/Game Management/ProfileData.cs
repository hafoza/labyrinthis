﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable()]
public class ProfileData
{
    public ProfileData()
    {
        numberOfCreatedProfiles = 0;
    }

    public int numberOfCreatedProfiles { get; set; }
    public Dictionary<string, int> profiles = new Dictionary<string, int>();

    public void newProfile(string name)
    {
        numberOfCreatedProfiles++;
        profiles.Add(name, numberOfCreatedProfiles);
    }

    public void removeProfile(string name)
    {
        numberOfCreatedProfiles--;
        profiles.Remove(name);
    }
    public bool profileExists(string name)
    {
        return profiles.ContainsKey(name);
    }
}
