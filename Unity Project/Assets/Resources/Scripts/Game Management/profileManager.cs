﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System;
using System.Collections.Generic;

[RequireComponent(typeof(UIProfileManager))]
public class ProfileManager : MonoBehaviour
{

    public static ProfileManager instance = null;

    [HideInInspector]
    public string path;

    public Serialization serialization;
    public Profile currentProfile;
    public ProfileData profileData;


    public void Awake()
    {
        instance = null;
        //Sets up the singleton pattern
        // TODO: Deal with Instantiation for singletons
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        serialization = GameManager.instance.GetComponent<Serialization>();

        path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\My Games\\Labyrinthis\\";
        Directory.CreateDirectory(path);

        if (File.Exists(path + "profileData.prfs"))
            LoadProfileData();
        else
            SaveProfileData();
    }

    public void SaveProfileData()
    {
        serialization.Serialize(path + "profileData.prfs", profileData);
    }

    public void LoadProfileData()
    {
        serialization.Deserialize<ProfileData>(path + "profileData.prfs", ref profileData);
        //HACK
        GetComponent<UIProfileManager>().InstantiateProfileButtons();
    }

    public void LoadProfileInfo(string name)
    {
        serialization.Deserialize<Profile>(String.Format("{0}\\{1}\\profileInfo.info", path, name), ref currentProfile);
    }

    public void SaveProfileInfo(Profile profile)
    {
        serialization.Serialize(String.Format("{0}\\{1}\\profileInfo.info", path, profile.name), profile);
    }

    public bool CreateProfile(string name)
    {
        if (!profileData.profileExists(name))
        {
            Profile profile = new Profile(name);
            profileData.newProfile(profile.name);
            profile.id = profileData.numberOfCreatedProfiles;

            Directory.CreateDirectory(path + "\\" + profile.name);

            SaveProfileInfo(profile);

            SaveProfileData();

            currentProfile = profile;
            return true;
        }

        return false;
    }

    public bool DeleteProfile(Profile profile)
    {
        if (Directory.Exists(path + "\\" + profile.name))
        {
            Directory.Delete(path + "\\" + profile.name);
            profileData.removeProfile(profile.name);
            SaveProfileData();
            return true;
        }
        return false;
    }
}
