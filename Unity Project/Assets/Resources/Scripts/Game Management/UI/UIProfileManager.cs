﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent (typeof (ProfileManager))]
public class UIProfileManager : MonoBehaviour 
{
    public InputField nameInput;
    public GameObject ButtonPrefab;
    public GameObject scrollViewContent;

    string UIProfileName;

    void Start()
    {
        
    }

    public void UpdateProfileName()
    {
        UIProfileName = nameInput.text;
    }

    public void UICreateProfile()
    {
        if (UIProfileName != "")
        {
            if (ProfileManager.instance.CreateProfile(UIProfileName))
            {
                ProfileManager.instance.currentProfile.descendFloor();
            }
            else
                print("Name already in use");
        }
    }

    public void InstantiateProfileButtons()
    {
        foreach (KeyValuePair<string, int> entry in ProfileManager.instance.profileData.profiles)
        {
            GameObject tempObj = GameObject.Instantiate(ButtonPrefab);

            tempObj.GetComponentInChildren<Text>().text = entry.Key;
            tempObj.GetComponent<profileLoadingButton>().profileName = entry.Key;
            tempObj.GetComponent<profileLoadingButton>().ProfileManager = ProfileManager.instance;
            tempObj.GetComponent<RectTransform>().SetParent(scrollViewContent.transform, false);
        }
    }

}
