﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject playerAvatar;

    public static GameManager instance = null;

    void Awake()
    {
        instance = null;
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

    }


    void OnEnable()
    {
        SceneManager.activeSceneChanged += OnSceneChange;
        SceneManager.activeSceneChanged += GetComponent<LevelGeneration>().OnSceneChange;
    }

    void OnDisable()
    {
        SceneManager.activeSceneChanged -= OnSceneChange;
        SceneManager.activeSceneChanged -= GetComponent<LevelGeneration>().OnSceneChange;
    }

    private void OnSceneChange(Scene scene, Scene newScene)
    {
        if (GameObject.FindGameObjectWithTag("PlayerAvatar") != null)
            playerAvatar = GameObject.FindGameObjectWithTag("PlayerAvatar");
    }

}
