﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[System.Serializable()]
public class Profile {

    public Profile(string name_)
    {
        name = name_;
    }

    public string name { get; private set; }
    public int id { get; set; }

    [System.Serializable()]
    public struct MetaGameStats
    {
        public int monstersKilled;
        public int secretsFound;
        public int bossesKilled;

    }

    int currentLevel = 0;
    float[] playerPosition;
    public bool isOnHighestLevel;
    public int highestLevelDescended;
    public int highestLevelGenerated;

    public void setCurrentLevel(int level)
    {
        currentLevel = level;
    }

    public void descendFloor()
    {
        currentLevel++;
        if (currentLevel <= highestLevelDescended)
        {
            isOnHighestLevel = false;
            ProfileManager.instance.SaveProfileInfo(this);
            SceneManager.LoadScene("testScene", LoadSceneMode.Single);
            GameManager.instance.GetComponent<Serialization>().SaveLevelData(this);
        }

        if (currentLevel > highestLevelDescended)
        {
            highestLevelDescended = currentLevel;
            isOnHighestLevel = true;
            ProfileManager.instance.SaveProfileInfo(this);
            SceneManager.LoadScene("testScene", LoadSceneMode.Single);
            //GameManager.instance.GetComponent<serialization>().SaveLevelData(this);
        }

    }

    public void loadGame()
    {
        ProfileManager.instance.SaveProfileInfo(this);
        SceneManager.LoadScene("testScene", LoadSceneMode.Single);
        GameManager.instance.GetComponent<Serialization>().SaveLevelData(this);
    }

    public void goToLevel(int level)
    {
        if (level > 0 && level <= highestLevelGenerated)
        {
            currentLevel = level;
            SceneManager.LoadScene("testScene", LoadSceneMode.Single);
        }
    }

    public void ascendFloor()
    {
        if (currentLevel > 1)
        {
            isOnHighestLevel = false;
            currentLevel--;
            SceneManager.LoadScene("testScene", LoadSceneMode.Single);
        }
    }

    public int getCurrentLevel()
    {
        return currentLevel;
    }

    public void updatePlayerPosition(float[] currentPlayerPosition)
    {
        playerPosition = currentPlayerPosition;
    }

    public float[] getPlayerposition()
    {
        return playerPosition;
    }

    public string returnProfileData()
    {
        string profileData;
        updatePlayerPosition(new float[3] { 0, 0, 0 });
        float[] pp = getPlayerposition();
        profileData = string.Format(" Name: {0} \n ID: {1} \n Current Level: {2} \n Player Position: {3:0.00}, {4:0.00}, {5:0.00} \n Highest Level Descended: {6} \n Highest Level Generated: {7}\n",
            name, id, getCurrentLevel(), pp[0], pp[1], pp[2], highestLevelDescended, highestLevelGenerated);

        return profileData;
    }

}
