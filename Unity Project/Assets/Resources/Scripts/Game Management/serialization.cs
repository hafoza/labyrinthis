﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System;

public class Serialization : MonoBehaviour {

    public string path;
    public levelData currentlevelData;
    public ProfileManager ProfileManager;

    public void Start()
    {
        currentlevelData = new levelData();
        path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\My Games\\Labyrinthis\\";
        Directory.CreateDirectory(path);
    }

    public void Serialize(string path, System.Object objectToSerialize)
    {
        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
        formatter.Serialize(stream, objectToSerialize);
        stream.Close();
    }

    public void Serialize(string path, System.Object objectToSerialize, FileMode fileMode)
    {
        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(path, fileMode, FileAccess.Write, FileShare.None);
        formatter.Serialize(stream, objectToSerialize);
        stream.Close();
    }

    public void Deserialize<T>(string path, ref T objectToDeserialize)
    {
        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
        objectToDeserialize = (T)formatter.Deserialize(stream);
        stream.Close();
    }

    public void SaveLevelData(Profile profile)
    {
        levelData[] oldlevelData = new levelData[profile.highestLevelDescended + 1];
        for (int i = 1; i <= profile.highestLevelDescended; i++)
        {
            oldlevelData[i] = DeserializeLevel(i, profile.name);
        }
        Stream stream = new FileStream(String.Format("{0}\\{1}\\saveFile.sav", path, profile.name), FileMode.Create, FileAccess.Write, FileShare.None);
        stream.Close();

        for (int i = 1; i <= profile.highestLevelDescended; i++)
        {
            SerializeLevel(oldlevelData[i], profile.name);
        }
    }

    public void replaceLevel(int level, levelData replacement, Profile profile)
    {
        levelData[] oldlevelData = new levelData[profile.highestLevelDescended + 1];
        for (int i = 1; i <= profile.highestLevelDescended; i++)
        {
            if (i != level)
            {
                oldlevelData[i] = DeserializeLevel(i, profile.name);
            }
            else
            {
                oldlevelData[i] = replacement;
            }
        }
        Stream stream = new FileStream(String.Format("{0}\\{1}\\saveFile.sav", path, name), FileMode.Create, FileAccess.Write, FileShare.None);
        stream.Close();

        for (int i = 1; i <= profile.highestLevelDescended; i++)
        {
            SerializeLevel(oldlevelData[i], profile.name);
        }
    }

    public void SerializeLevel(levelData levelToSerialize, string profileName)
    {
        Serialize(String.Format("{0}\\{1}\\saveFile.sav", path, profileName), levelToSerialize, FileMode.Append);
    }

    public levelData DeserializeLevel(int level, string profileName)
    {
        bool foundLevel = false;
        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(String.Format("{0}\\{1}\\saveFile.sav", path, profileName), FileMode.Open, FileAccess.Read, FileShare.Read);
        while (stream.Position != stream.Length && !foundLevel)
        { 
            var lData = formatter.Deserialize(stream) as levelData; 
            if(lData != null)
            { 
                if (lData.returnLevel() == level)
                {
                    foundLevel = true;
                }
                currentlevelData = lData;
            }
        }
        stream.Close();

        return currentlevelData;
    }

	
}
