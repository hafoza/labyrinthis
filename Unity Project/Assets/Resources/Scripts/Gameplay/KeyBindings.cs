﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KeyBindings : MonoBehaviour {

    private Dictionary<string, KeyCode> DefaultBindings;
    public Dictionary<string, KeyCode> CurrentBindings;

    void Awake()
    {
        DefaultBindings = new Dictionary<string, KeyCode>();

        DefaultBindings.Add("Run", KeyCode.LeftShift);
        DefaultBindings.Add("Dodge", KeyCode.Space);
        DefaultBindings.Add("Inventory", KeyCode.I);
        DefaultBindings.Add("Lore", KeyCode.L);

        if (CheckForCustomKeybindings() == false)
        {
            CurrentBindings = DefaultBindings;
        }

    }

    bool CheckForCustomKeybindings()
    {
        //TODO: Add logic for saving custom keybindings

        return false;
    }

}
