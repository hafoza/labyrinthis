﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputHandler : MonoBehaviour {

    public bool RegisterInputs;
    public KeyBindings keyBindings;

    public Dictionary <string, bool> ActiveKeys;

    void Awake()
    {
        ActiveKeys = new Dictionary<string, bool>();

        RegisterInputs = true;
        foreach (string input in keyBindings.CurrentBindings.Keys)
        {
            ActiveKeys.Add(input, false);
        }
    }

    void Update()
    {

        if(RegisterInputs == true)
        { 
            foreach (string input in keyBindings.CurrentBindings.Keys)
            {
                if (Input.GetKeyUp(keyBindings.CurrentBindings[input]))
                    ActiveKeys[input] = false;
                if (Input.GetKeyDown(keyBindings.CurrentBindings[input]))
                    ActiveKeys[input] = true;
            }
        }
    }

}